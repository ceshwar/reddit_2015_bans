###psm = read.csv("PSM_DATA/matching_ct_data_all_features_nonZero.csv", header=TRUE)
#psm = read.csv("PSM_DATA/matching_ct_data_all_features.csv", header=TRUE)
psm = read.csv("MDM_new_data/top200_mdm_ct_data_all_features_final.csv", header=TRUE)
#psm = read.csv("MDM_new_data/mdm_ct_subsample_data_all_features_final.csv", header=TRUE)

library(MatchIt)
library(dplyr)
library(ggplot2)

psm = psm[,c("user", "age","preban", "karma", "treatment")]

##try PSM
#m = matchit(treatment ~ preban + age + karma, method = "nearest", data = psm, ratio=1 )
m = matchit(treatment ~ preban + age + karma, method = "nearest", distance = "mahalanobis", data = psm, ratio=1 )

final = match.data(m)

#write.csv (final, file="PSM_RESULTS/ct_psm_matched_all_features_all_data.csv")
write.csv (final, file="MDM_new_results/top200_ct_mdm_matched_all_features_all_data.csv")
#write.csv (final, file="MDM_new_results/ct_mdm_matched_all_features_all_data.csv")

#write.csv(m$match.matrix, file="PSM_RESULTS/ct_psm_matched_all_features_users.csv")
write.csv(m$match.matrix, file="MDM_new_results/top200_ct_mdm_matched_all_features_users.csv")
#write.csv(m$match.matrix, file="MDM_new_results/ct_mdm_matched_all_features_users.csv")
