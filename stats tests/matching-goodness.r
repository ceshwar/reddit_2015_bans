library(MatchIt)
library(dplyr)
library(ggplot2)

#dta_m2 = read.csv('/home/eshwar/Desktop/top200_ct_mdm_matched_all_features_all_data.csv')
dta_m2 = read.csv('/home/eshwar/Desktop/research/reddit-bans-cscw2018/reddit-results/PSM_results/fixed_rq1_match/top200_ct_mdm_matched_all_features_all_data.csv')
dta_m2 = dta_m2[,c('user', 'preban', 'age', 'karma','treatment', 'weights')]

wilcox.test(dta_m2[,('preban')] ~ dta_m2$treatment)
wilcox.test(dta_m2[,('karma')] ~ dta_m2$treatment)
wilcox.test(dta_m2[,('age')] ~ dta_m2$treatment)

#dta_m2 = read.csv('/home/eshwar/Desktop/top200_fph_mdm_matched_all_features_all_data.csv')
dta_m2 = read.csv('/home/eshwar/Desktop/research/reddit-bans-cscw2018/reddit-results/PSM_results/fixed_rq1_match/top200_fph_mdm_matched_all_features_all_data.csv')
dta_m2 = dta_m2[,c('user', 'preban', 'age', 'karma','treatment', 'weights')]

wilcox.test(dta_m2[,('preban')] ~ dta_m2$treatment)
wilcox.test(dta_m2[,('karma')] ~ dta_m2$treatment)
wilcox.test(dta_m2[,('age')] ~ dta_m2$treatment)

fn_bal <- function(dta, variable) {
  dta$variable <- dta[, variable]
  dta$treatment <- as.factor(dta$treatment)
  support <- c(min(dta$variable), max(dta$variable))
  ggplot(dta, aes(x = distance, y = variable, color = treatment, group=1)) +
    geom_point(alpha = 0.2, size = 1.3) +
    geom_smooth(method = "loess", se = F) +
    xlab("Propensity score") +
    ylab(variable) +
    theme_bw() +
    ylim(support)
}

library(gridExtra)
grid.arrange(
  fn_bal(dta_m2, "preban"),
  fn_bal(dta_m2, "karma"),
  fn_bal(dta_m2, "age") + theme(legend.position = "none"),
  nrow = 3, widths = c(1, 0.8)
)

library(gridExtra)
grid.arrange(fn_bal(dta_m2, "age"),
             nrow = 3, widths = c(1, 0.8))
