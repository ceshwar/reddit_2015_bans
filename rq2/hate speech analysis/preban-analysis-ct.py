###temporal analysis window size = 1
#####pre-ban hate speech usage for treatment CT; split by monthly chunks till dec 2015
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from glob import glob
import csv
import os
import json
import codecs
import re
import pandas as pd
import time
from datetime import datetime, timedelta

#fit SAGE lexicon into Vectorizer
#sage_lex = pd.read_csv('lexicon/sage-outputfatpeoplehate.csv')
sage_lex = pd.read_csv('../lexicon/filtered-sage-ct.csv')
#cv = CountVectorizer(vocabulary = sage_lex[sage_lex.sage > 2.0].word)
cv = CountVectorizer(vocabulary = sage_lex[:100].word)

##labelled words
lab = pd.read_csv('../lexicon/labelled-ct-lexicon.csv', names = ['word'])
#lab = lab[['word','average_score']][:100]
#lab = lab[lab.average_score > 0]
label = CountVectorizer(vocabulary = lab.word)

##select which entity to perform analysis for!
#entity = "treatment-user"
#entity = "control-user"
#entity = "treatment-sub-new"
entity = "treatment-sub-old"

#set time-window for analysis: units = days
#timewindow = 1
timewindow = 10
#timewindow = 30
offset = 0

#hate = 0	###control users only
hate = 1	###hate users only
user_list = pd.read_csv('../handles/treatment-ct.csv', names= ['user']).user

###load user names - sorted by activity; top most users are the most active
if entity == "treatment-user":
        fph_users = pd.read_csv('../handles/treatment-ct.csv', names = ['user'])
        #load timelines
        filepath = "/mnt/storage/eshwar3/reddit-coontown-treatment-users/"
	preban = "coontown_user_comments_jan_to_may_2015_new/" 
	preNpostban = "coontown_user_comments_jun_to_aug_2015/"
	postban2 = "coontown_user_comments_sep_to_dec_2015/"
elif entity == "control-user":
        fph_users = pd.read_csv('../handles/mdm_control_ct.csv', names = ['user'])
        #load timelines
        filepath = "/mnt/storage/eshwar3/mdm-control-users/"
        preban = "mdm_control_ct_comments_jan_to_may_2015/"
        preNpostban = "mdm_control_ct_comments_jun_to_aug_2015/"
        postban2 = "mdm_control_ct_comments_sep_to_dec_2015/"
elif entity == "treatment-sub-new":
        fph_users = pd.read_csv('../handles/treatment-subs-ct-new.csv', names = ['user'])
        #load timelines
        filepath = "/mnt/storage/eshwar3/reddit-ct-treatment-subs/"
        preban = "new-treatment-subs-ct_jan_to_may_2015/"
        preNpostban = "new-treatment-subs-ct_jun_to_aug_2015/"
        postban2 = "new-treatment-subs-ct_sep_to_dec_2015/"
elif entity == "treatment-sub-old":
        fph_users = pd.read_csv('../handles/treatment-subs-ct-old.csv', names = ['user'])
        #load timelines
        filepath = "/mnt/storage/eshwar3/reddit-ct-treatment-subs/"
        preban = "sorted_treatment_ct_activityIncrease_over25percent_jan_to_may_2015/"
        preNpostban = "sorted_treatment_ct_activityIncrease_over25percent_jun_to_aug_2015/"
        postban2 = "sorted_treatment_ct_activityIncrease_over25percent_sep_to_dec_2015/"

#load user names - sorted by activity; top most users are the most active
#fph_users = pd.read_csv('handles/treatment-ct.csv', names = ['user'])
#fph_users = fph_users[fph_users.user != 'AutoModerator']
#fph_users = fph_users[fph_users.user != '[deleted]']

#load timelines
#filepath = "/mnt/storage/eshwar3/reddit-coontown-treatment-users/"
#preban = "coontown_user_comments_jan_to_may_2015/"
#preNpostban = "coontown_user_comments_jun_to_aug_2015/"
#postban2 = "coontown_user_comments_sep_to_dec_2015/"

increase = 0
decrease = 0

##create lists to store the values for each user in the dataframe
preban_hate = []
preban_total = []
postban_hate = []
postban_total = []
ratios = []

##time-windows
import time
from datetime import datetime, timedelta
# created_UTC from the last post ever made on r/CoonTown
t_ban = datetime.utcfromtimestamp(1437686745)

##set time-window for analysis: units = days
#timewindow = 1
#timewindow = 30
#offset = 0

###store the corresponding hate speech usage for each window size
window_n = []
hate_usage_n = []
volume_n = []
totalWords_n = []
labeled_n = []

###limit is the last post-ban date, from the T-ban, that is used to collect posts by users.
if timewindow == 1:
        limit = t_ban - timedelta(30)
else:
        limit = t_ban.replace(year = 2014, month = 12, day = 31)
#       limit = t_ban.replace(year = 2015, month = 1, day = 1)


###no. of users to analyze
#top_K = 5
top_K = len(fph_users)
overall_usage = pd.DataFrame()
usage = pd.DataFrame()
count = 0
misses = 0
##tokenizer to get the number of words in the sentence
from nltk.tokenize import RegexpTokenizer
tokenizer = RegexpTokenizer(r'\w+')
##

for username in fph_users[:top_K].user:
    print username
    print "#users left = ", (top_K - count)
    count += 1
    ####store the corresponding hate speech usage for each window size
    window_n = []
    hate_usage_n = []
    volume_n = []
    offset = 0
    totalWords_n = []
    labeled_n = []

    #### count number of occurrences pre- and post- ban
#    file_name = filepath + preNpostban + "user_comments_" + username + ".json"
    if entity == "treatment-user":
        file_name = filepath + preNpostban + "user_comments_" + username + ".json"
    elif entity == "control-user":
        file_name = filepath + preNpostban + "user_comments_" + username + ".json"
    else:
        file_name = filepath + preNpostban + "subr_comments_" + username + ".json"

    with codecs.open(file_name, encoding = "utf8") as f:
        data = f.readlines()
    data = map(lambda x: x.rstrip(), data)
    data_json_str = "[" + ','.join(data)+"]"
    df = pd.read_json(data_json_str)
    print len(df)
    temp = df
    #file_name = filepath + preban + "user_comments_" + username + ".json"
    if entity == "treatment-user":
        file_name = filepath + preban + "user_comments_" + username + ".json"
    elif entity == "control-user":
        file_name = filepath + preban + "user_comments_" + username + ".json"
    else:
        file_name = filepath + preban + "subr_comments_" + username + ".json"

    with codecs.open(file_name, encoding = "utf8") as f:
        data = f.readlines()
    data = map(lambda x: x.rstrip(), data)
    data_json_str = "[" + ','.join(data)+"]"
    df = pd.read_json(data_json_str)
    print len(df)
    df = pd.concat([df,temp])
    if len(df) == 0:
        misses += 1
        continue
    try:
	if entity == "treatment-user":
		df = df[df.author == username]
	elif entity == "control-user":
		df = df[df.author == username]
	else:
		df = df[df.subreddit == username]
    except:
        misses += 1
        continue
    ###only use posts by treatment users
    if hate == 1:
	df = df[df['author'].isin(user_list)]
    else:
	df = df[~df['author'].isin(user_list)]

    print len(df)
    ####

    if len(df) == 0:
        misses += 1
        continue

    prebanhate = 0
    prebantotal = 0

    while t_ban - timedelta(offset) >= limit:
        prebanhate_iter = 0
        prebantotal_iter = 0
        prebanWords_iter = 0
        prebanLabeled_iter = 0

        iteration = float(offset)/timewindow
#         print "Iteration: ", iteration
        window_n.append(-1 * offset)
#         print t_ban + timedelta(offset)
#         df1 = df[datetime.utcfromtimestamp(df.created_utc) > t_ban]
        df1 = df[(df['created_utc'].apply(datetime.utcfromtimestamp) < t_ban - timedelta(offset))&(df['created_utc'].apply(datetime.utcfromtimestamp) > t_ban - timedelta(timewindow + offset))]
#             if sub.created_utc > t_ban + offset & sub.created_utc < t_ban + timewindow + offset:

        offset += timewindow
        try:
            arr = cv.fit_transform(df1.body).toarray()
#             print "Hate usage = ", np.sum(arr)
#             print "Total posts = ", len(arr)
            prebanhate_iter += np.sum(arr)
            prebantotal_iter += len(arr)
            prebanhate += np.sum(arr)
            prebantotal += len(arr)
            for text in df1.body:
                prebanWords_iter += len(tokenizer.tokenize(text))
            arr = label.fit_transform(df1.body).toarray()
            prebanLabeled_iter += np.sum(arr)
            
        except:
            print "Blah errors!"
        hate_usage_n.append(prebanhate_iter)
        volume_n.append(prebantotal_iter)
        totalWords_n.append(prebanWords_iter)
        labeled_n.append(prebanLabeled_iter)
            ##convert into pandas dataframe
    usernam = []
    for i in range(0,len(window_n)):
        usernam.append(username)
    usage['user'] = usernam
    usage['day'] = window_n
    usage['hate_speech'] = hate_usage_n
    usage['total_posts'] = volume_n
    usage['total_words'] = totalWords_n
    usage['labeled_hate'] = labeled_n

    overall_usage = pd.concat([overall_usage,usage])

if hate == 0:
	overall_usage.to_csv('old-resident-rq3-preban-analysis-ct-' + entity + '-windowsize-'+ str(timewindow) +'.csv', index = False, encoding = "utf8")
else:
	overall_usage.to_csv('new-migrant-rq3-preban-analysis-ct-' + entity + '-windowsize-'+ str(timewindow) +'.csv', index = False, encoding = "utf8")
print "Its done(preban temporal)- " + entity + " , bin size = " + str(timewindow)
print 'misses = ', misses
