#postban temporal analysis windowsize = 30 or 1
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from glob import glob
import csv
import os
import json
import codecs
import re
import pandas as pd
import time
from datetime import datetime, timedelta

#fit SAGE lexicon into Vectorizer
#sage_lex = pd.read_csv('lexicon/sage-outputfatpeoplehate.csv')
sage_lex = pd.read_csv('../lexicon/filtered-sage-fph.csv')
#cv = CountVectorizer(vocabulary = sage_lex[sage_lex.sage > 2.0].word)
cv = CountVectorizer(vocabulary = sage_lex[:100].word)

##labelled words
lab = pd.read_csv('../lexicon/labelled-fph-lexicon.csv', names = ['word'])
#lab = lab[['word','average_score']][:100]
#lab = lab[lab.average_score > 0]
label = CountVectorizer(vocabulary = lab.word)

##select which entity to perform analysis for!
#entity = "treatment-user"
#entity = "control-user"
#entity = "treatment-sub-new"
entity = "treatment-sub-old"

#set time-window for analysis: units = days
#timewindow = 1
timewindow = 10
#timewindow = 30
offset = 0

#hate = 0 ##control
hate = 1 ##hate user

user_list = pd.read_csv('../handles/treatment-fph.csv', names= ['user']).user


###load user names - sorted by activity; top most users are the most active
if entity == "treatment-user":
        fph_users = pd.read_csv('handles/treatment-fph.csv', names = ['user'])
        #load timelines
        filepath = "/mnt/storage/eshwar3/reddit-fph-treatment-users/"
        preban = "fph_user_comments_jan_to_may_2015_new/"
        postban1 = "fph_user_comments_june_aug/"
        postban2 = "fph_user_comments_sep_dec_2015/"
elif entity == "control-user":
        fph_users = pd.read_csv('handles/mdm_control_fph.csv', names = ['user'])
        #load timelines
        filepath = "/mnt/storage/eshwar3/mdm-control-users/"
        preban = "mdm_control_fph_comments_jan_to_may_2015/"
        postban1 = "mdm_control_fph_comments_jun_to_aug_2015/"
        postban2 = "mdm_control_fph_comments_sep_to_dec_2015/"
elif entity == "treatment-sub-new":
        fph_users = pd.read_csv('../handles/treatment-subs-fph-new.csv', names = ['user'])
        #load timelines
        filepath = "/mnt/storage/eshwar3/reddit-fph-treatment-subs/"
        preban = "infiltrated_comments_jan_to_may_2015/"
        postban1 = "infiltrated_comments_jun_to_aug_2015/"
        postban2 = "infiltrated_comments_sep_to_dec_2015/"
elif entity == "treatment-sub-old":
        fph_users = pd.read_csv('../handles/treatment-subs-fph-old.csv', names = ['user'])
        #load timelines
        filepath = "/mnt/storage/eshwar3/reddit-fph-treatment-subs/"
        preban = "sorted_treat_jan_to_may_2015/"
        postban1 = "sorted_treat_june_to_aug_2015/"
        postban2 = "sorted_treat_sep_to_dec_2015/"

###load user names - sorted by activity; top most users are the most active
#fph_users = pd.read_csv('handles/treatment-fph.csv', names = ['user'])
#fph_users = fph_users[fph_users.user != 'AutoModerator']

###load timelines
#filepath = "/mnt/storage/eshwar3/reddit-fph-treatment-users/"
#preban = "fph_user_comments_jan_to_may_2015_new/"
#postban1 = "fph_user_comments_june_aug/"
#postban2 = "fph_user_comments_sep_dec_2015/"

increase = 0
decrease = 0

##create lists to store the values for each user in the dataframe
preban_hate = []
preban_total = []
postban_hate = []
postban_total = []
ratios = []
deleted_user = 0
deleted_handles = []

##time-windows
import time
from datetime import datetime, timedelta
# created_UTC from the last post ever made on r/fatpeoplehate
#t_ban = datetime.utcfromtimestamp(1433155808)
####UPDATED -> Ban on Jun 10, 2015
t_ban = datetime.utcfromtimestamp(1433155808) + timedelta(8.55)

##set time-window for analysis: units = days
#timewindow = 1
#timewindow = 30
#offset = 0

if timewindow == 1:
    limit = t_ban + timedelta(30)
else:
    limit = t_ban.replace(year = 2016, month = 1, day = 1)
    
###no. of users to analyze
#top_K = 10
top_K = len(fph_users)
overall_usage = pd.DataFrame()
usage = pd.DataFrame()
count = 0

##tokenizer to get the number of words in the sentence
from nltk.tokenize import RegexpTokenizer
tokenizer = RegexpTokenizer(r'\w+')
##

skip = ['fo4','fantasyfootball', 'CFB']

for username in fph_users[:top_K].user:
    ####store the corresponding hate speech usage for each window size
    window_n = []
    hate_usage_n = []
    volume_n = []
    offset = 0
    totalWords_n = []
    labeled_n = []
    print username
    print "#users left = ", (top_K - count)
    if username in skip:
	print "Skipping!"
	continue
    count += 1
    ####post-ban phase1 (jun-aug)
#    file_name = filepath + postban1 + "user_comments_" + username + ".json"
    if entity == "treatment-user":
        file_name = filepath + postban1 + "user_comments_" + username + ".json"
    elif entity == "control-user":
        file_name = filepath + postban1 + "user_comments_" + username + ".json"
    else:
        file_name = filepath + postban1 + "subr_comments_" + username + ".json"

    with codecs.open(file_name, encoding = "utf8") as f:
        data = f.readlines()
    data = map(lambda x: x.rstrip(), data)
    data_json_str = "[" + ','.join(data)+"]"
    df = pd.read_json(data_json_str)
    print len(df)
    temp = df
    ####post-ban phase2 (sep-dec)
#    file_name = filepath + postban2 + "user_comments_" + username + ".json"
    if entity == "treatment-user":
        file_name = filepath + postban2 + "user_comments_" + username + ".json"
    elif entity == "control-user":
        file_name = filepath + postban2 + "user_comments_" + username + ".json"
    else:
        file_name = filepath + postban2 + "subr_comments_" + username + ".json"

    with codecs.open(file_name, encoding = "utf8") as f:
        data = f.readlines()
    data = map(lambda x: x.rstrip(), data)
    data_json_str = "[" + ','.join(data)+"]"
    df = pd.read_json(data_json_str)
    print len(df)
    df = pd.concat([temp,df])
    print len(df)
    if hate == 1:
	df = df[df['author'].isin(user_list)]
    else:
	df = df[~df['author'].isin(user_list)]
    print len(df)

    if len(df) == 0:
        print "No posts made post-ban"
        deleted_user += 1
        deleted_handles.append(username)
        continue
        
    postbanhate = 0
    postbantotal = 0
    while t_ban + timedelta(offset) < limit:
        postbanhate_iter = 0
        postbantotal_iter = 0
        prebanWords_iter = 0
        prebanLabeled_iter = 0
        iteration = float(offset)/timewindow
#         print "Iteration: ", iteration
        window_n.append(offset)
#         print t_ban + timedelta(offset)
#         df1 = df[datetime.utcfromtimestamp(df.created_utc) > t_ban]
        df1 = df[(df['created_utc'].apply(datetime.utcfromtimestamp) > t_ban + timedelta(offset))&(df['created_utc'].apply(datetime.utcfromtimestamp) < t_ban + timedelta(timewindow + offset))]
#             if sub.created_utc > t_ban + offset & sub.created_utc < t_ban + timewindow + offset:

        offset += timewindow
        try:
            arr = cv.fit_transform(df1.body).toarray()
#             print "Hate usage = ", np.sum(arr)
#             print "Total posts = ", len(arr)
            postbanhate_iter += np.sum(arr)
            postbantotal_iter += len(arr)
            postbanhate += np.sum(arr)
            postbantotal += len(arr)
            for text in df1.body:
                prebanWords_iter += len(tokenizer.tokenize(text))
            arr = label.fit_transform(df1.body).toarray()
            prebanLabeled_iter += np.sum(arr)
        except:
            print "Blah errors!"
        hate_usage_n.append(postbanhate_iter)
        volume_n.append(postbantotal_iter)
        totalWords_n.append(prebanWords_iter)
        labeled_n.append(prebanLabeled_iter)
    ##convert into pandas dataframe
#     print "Outta loop"
    usernam = []
    for i in range(0,len(window_n)):
        usernam.append(username)
    usage['user'] = usernam
    usage['day'] = window_n
    usage['hate_speech'] = hate_usage_n
    usage['total_posts'] = volume_n
    usage['total_words'] = totalWords_n
    usage['labeled_hate'] = labeled_n
    overall_usage = pd.concat([overall_usage,usage])

if hate == 0:
	overall_usage.to_csv('old-resident-rq3-postban-analysis-fph-' + entity + '-windowsize-'+ str(timewindow) +'.csv', index = False, encoding = "utf8")
else:
	overall_usage.to_csv('new-migrant-rq3-postban-analysis-fph-' + entity + '-windowsize-'+ str(timewindow) +'.csv', index = False, encoding = "utf8")

print "Its done for " + entity + "! bin size = " + str(timewindow)
print "#deleted users = ", deleted_user
print deleted_handles
#del_handles = pd.DataFrame()
#del_handles['users'] = deleted_handles
#del_handles.to_csv('postban-inactive-fph-' + entity + 'bin-' + str(timewindow) + '.csv',index = False, header=None)
