# README #

Code for data preparation and analysis for the CSCW 2018 paper: "You Can’t Stay Here: The Efficacy of Reddit’s 2015 Ban Examined Through Hate Speech"

* ACM Reference: 
Eshwar Chandrasekharan, Umashanthi Pavalanathan, Anirudh Srinivasan, Adam Glynn, Jacob Eisenstein,
and Eric Gilbert. 2017. You Can’t Stay Here: The Efficacy of Reddit’s 2015 Ban Examined Through Hate Speech.
Proc. ACM Hum.-Comput. Interact. 1, 1, Article 31 (January 2017), 22 pages.
https://doi.org/10.1145/3134666

* Repo owner or admin: Eshwar Chandrasekharan (eshwar3 [aT] gatech [DoT] edu)