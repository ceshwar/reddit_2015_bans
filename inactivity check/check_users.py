##get control user characteristics
import pandas as pd
import time
import numpy as np
import praw
# USER_HANDLE = "MaddGerman"
USER_AGENT = "python:reddithate:v0.3 (by /u/eshwar)" # user agent for the application
# USER_AGENT = "python:automod:v0.1 (by /u/hide_ous)" # user agent for the application
r = praw.Reddit(USER_AGENT)

filepath = "../handles/"
#filename = "top200_mdm_control_fph.csv"
filename = "top200_mdm_control_ct.csv"
#filename = "mdm_control_ct.csv"
#filename = "mdm_control_fph.csv"
#filename = "treatment-fph.csv"
#filename = "treatment-ct.csv"
suffix = filename[:-4]
users = pd.read_csv(filepath+filename, names = ['user'])
count = 0
miss = 0

tot = len(users)
deleted_users = []

##sleep to prevent rate-limiting
countdown = 0
SECONDS = 1
MINUTES = 60*SECONDS
DEFAULT_SLEEP_TIME = 30 # how long to sleep if errors happen
# DEFAULT_SLEEP_TIME = 1 * MINUTES # how long to sleep if errors happen
##

for user in users.user:
#     if cnt == 0:  
#         sub_c.set_value(i,'#subscribers',2)
#         cnt = 1
    count = count + 1
    print str(tot - count) + ' users to go!'
#     if count > 10:
#         break
    try:
            non_lazy_user = r.get_redditor(user, fetch=True)
            print "Created on: ", non_lazy_user.created_utc

    except praw.errors.NotFound:
        print("Unable to find user")
        miss = miss + 1
        deleted_users.append(user)

    except:
        time.sleep(DEFAULT_SLEEP_TIME)
        print "Mandatory sleeping..."

print 'Total = ', tot
print 'Hits = ', count
print 'Miss = ', miss
pd.DataFrame(deleted_users).to_csv(suffix+'_deleted_users.csv', index = None, header = None)
print 'ITS DONE!'
